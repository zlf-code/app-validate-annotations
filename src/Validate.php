<?php
declare(strict_types=1);

namespace Zlf\AppValidateAnnotations;

use Attribute;
use Hyperf\Di\Annotation\AbstractAnnotation;

#[Attribute(Attribute::TARGET_METHOD)]
class Validate extends AbstractAnnotation
{
    public string $method = 'POST';

    /**
     * @var ?string 验证的模型类
     */
    public ?string $class;


    /**
     * 验证场景
     * @var string|null
     */
    public ?string $scene = null;

    /**
     * 验证规则
     * @var array
     */
    public array $rules = [];


    /**
     * 验证字段
     * @var array
     */
    public array $fields = [];


    /**
     * 验证字段
     * @var array
     */
    public array $labels = [];


    public function __construct(?string $class = null, array $rules = [], ?string $scene = null, array $fields = [], array $labels = [], string $method = 'POST')
    {
        $this->class = $class;
        $this->rules = $rules;
        $this->scene = $scene;
        $this->fields = $fields;
        $this->labels = $labels;
        $this->method = strtoupper($method);
    }
}